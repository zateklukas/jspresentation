# First Slide

Greetings, my name is Lukáš Zátek and I came here today to speak about Javascript. First of all, Have anyone of you ever made something in Javascript? and why did you hate it? It's kind of joke actually, few years ago everyone I met hated javascript and today it's the most popular language out there. To cite my colleague: "There are 3 major threats to this world, 1. Global Warming 2. World Hunger 3. Javascript. It got way better when they introduced ES5, but more on that later.

# Second Slide

Let me introduce you to Brendon Eich. This is the man, that introduced us to JavaScript in 1995. He is also co-founder and exCEO of Mozilla, which he departed from because of his homophobic statements on twitter. Then he founded Brave Software, which introduced Brave browser that has Adblocker by default and also Basic Attention Token, which is one of milion cryptocurrencies. Fun fact, do you know, how long it took Brendon Eich to make Javascript? Well... It's not javascript as we know it today, but Mocha. Javascript took approximately 20 years to evolve to the form we know it today.

# Third Slide

Javascript! It's lightweight interpreted of just-in-time compiled programming language with first-class functions. It has been found in 1995. Is multiparadigmical, which means we can use multiple paradigms in Javascript, like event-based, functional, object-oriented or prototype-based. It is using ECMAscript standards, which is currently at ES8 released in June 2018. ECMAScript is scripting-language specification, which serves as template for other languages as javascript to implement. It is dynamic typed, which basically means that you don't have to care about types you are using, compiler do that for you. Many people don't like this tho, if you are one of them, you can still use Typescript, which is defined as strict syntactical superset of JavaScript, which adds optional static typing to the language. And it's definitely not Java. Actually, Javascript was first called LiveScript, but was renamed in a deal between Netscape and Sun (now Oracle).

# Fourth Slide

Usages of Javascript. Javascript was originally scripting language for web pages. Over the course of years it changed drastically. Now you can make nearly anything you'd like from web pages, to mobile apps.

## Web development

Javascript on the web. It's very powerful tool used by nearly every webpage today to make them interactive. Few years ago, javascript was primary used as complementary tool to HTML and CSS to handle events, popups, send your browsing data and so on. You could use vanilla JavaScript or libraries like jQuery, which made life on Javascript developer easier. Since then, multiple Javascript frameworks has been introduces. Actually, It was a joke in like 2017, that every Startup had to make their own Javascript framework first. From the wast ocean of frameworks, I chose 3 most popular. Vue, React and Angular. Have anyone worked with them? ... What did you do? Can you share your work? Let's start with Angular first.

### Angular

Angular has been developed by Google, released in 2010. It is based on Typescript. Google introduces Angular 2 in 2016, which was huge shift in popularity. Integral part of all three frameworks are components. In Angular, components are referred to as directives, which are just markers on DOM elements. Angular can track and attach specific behaviour to them, separating UI part as HTML tags and behavioud in form of Javascript. It has a steep learning curve, because you have to learn concepts like TypeScript and MVC.

### React

React is backed by Facebook, released in 2013. It is used in Facebook, Instagram and WhatsApp. It combines the behaviour of components. The same part of code is responsible for creating a UI element and dictating its behaviour. It has a large community on StackOverflow, but it is not complete framework and advanced features require the use of third-party libraries. This maked it easier to learn then Angular.

### Vue

Vue is the youngest framework of those three. It's developed by ex-Google employee Evan You in 2014. Over the last two years Vue gained a lot of popularity, even tho it's not backed by any large company like Angular and React are. UI and behaviour in Vue are also part of components, which makes it similar to React. It is highly customizable, which allows you to combine the UI and behavior from a script. It the easiest to learn amongst those three. It has an overlap with Angular and React with respect to their functionality, so transition from either of the two is pretty easy as well. Howevery, simplicity and flexibility of Vue allows poor code and making it difficult to debug and test.

## Mobile development

For the mobile development, I chose 3 framework that can do the job. The first two - Cordova and Ionic are essentially web apps, that provide you methods to access native APIs of both Android and iOS. Basically, they make a WebView, where they run those web apps and it acts like native application.

React Native has pretty much the same syntax as React. It has been made by Facebook and it works differently than those two. It makes a bridge between Javascript and native, offering your predefined components that are then display as native components in Android and iOS, thus you get Native look on both platforms.

## Desktop development

NW.js and Electron

Electron is a framework developed and maintained by Github. It allows developement of desktop applications using web technologies. It combines Chromium rendering engine and Node.js runtime. Most of Electron APIs are written in C++ or Objective C and then they are exposed directly to app through JS bindings, so you can write entire app in JS. However, It's still web app and is vulnerable to web related attacks such as cross-site attacks.

## Server development

Node.js is an run-time enviroment that executes Javascript code outside of a browser. It allows you to run scripts server side to produce dynamic web pages. It has an event-driven arcitecture capable of asynchronous input/output operations.

# Examples

## Prototype

```javascript
function Person() {}

Person.prototype.id = 0;
Person.prototype.name = "";
Person.prototype.setName = function(name) {
  this.name = name;
};
var person = new Person();
person.setName("Lukáš");

//ES2015

class Person {
  constructor() {}
  setName(name) {
    this.name = name;
  }
}
```

## Weakly typed

```javascript
var num = 5;
var str = "String";
var dec = 5.5;
5 + "5";
5 == "5";
```

### what the Fs

```javascript
typeof []
[] == []
[] == ![]
0 > null
0 >= null
0 == null
0 <= null
0 < null
typeof null
null instanceof Object
```

## First - Class functions

```javascript
var fun = param => {
  console.log("Parameter", param);
};
fun("Lukáš");
```
