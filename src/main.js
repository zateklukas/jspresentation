import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import Hero from "vue-hero";

Vue.use(Hero);

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
